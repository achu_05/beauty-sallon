package com.example.beautyhub;

import com.google.firebase.auth.FirebaseAuth;

import java.util.Date;

public class Booking {
    String uid;

    public Booking(String service_time, String service_date, String service_type, String price) {
        this.serviceType=service_type;
        this.timestamp=new Date().getTime();
        this.uid= FirebaseAuth.getInstance().getUid();
        this.time=service_time;
        this.date=service_date;
        this.price=price;


    }


public Booking(){}

    public String getPrice() {
        return price;
    }

    public String getDate() {
        return date;
    }

    public Long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Long timestamp) {
        this.timestamp = timestamp;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getServiceType() {
        return serviceType;
    }

    public void setServiceType(String serviceType) {
        this.serviceType = serviceType;
    }



    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }



    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }
    String time;
    String serviceType;
    String date;
    Long timestamp;
    String price;
}
