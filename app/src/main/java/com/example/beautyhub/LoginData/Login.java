package com.example.beautyhub.LoginData;

import com.google.gson.annotations.SerializedName;


public class Login{

	@SerializedName("details")
	private Details details;

	@SerializedName("status")
	private String status;

	public void setDetails(Details details){
		this.details = details;
	}

	public Details getDetails(){
		return details;
	}

	public void setStatus(String status){
		this.status = status;
	}

	public String getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"Login{" + 
			"details = '" + details + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}
}