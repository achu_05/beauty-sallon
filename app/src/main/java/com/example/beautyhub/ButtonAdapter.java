package com.example.beautyhub;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


class ButtonAdapter extends RecyclerView.Adapter<ButtonAdapter.Holder> {

     String timeList[];
     Context ct;
     int index=-1;
     setTime listner;
     public ButtonAdapter(booking_next_page ct, String[] time) {
     this.timeList =time;
     listner=ct;
     this.ct = ct;
    }
    @NonNull
    @Override
    public ButtonAdapter.Holder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view= LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.custom_button,viewGroup,false);

        return new ButtonAdapter.Holder(view);

    }

    @Override
    public void onBindViewHolder(@NonNull ButtonAdapter.Holder holder, final int i) {
         if (i==index)
             holder.time.setBackgroundColor(0xffB90957);
         else
         {
             holder.time.setBackgroundColor(0xffEEE8E8);
         }
        holder.time.setText(timeList[i]);

    }

    @Override
    public int getItemCount() {
        return timeList.length;
    }


    public class Holder extends RecyclerView.ViewHolder {
        TextView time;
        public Holder(@NonNull View itemView) {
            super(itemView);
            time=itemView.findViewById(R.id.timebutton);
            time.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listner.onSetTime(timeList[getAdapterPosition()]);
                    index=getAdapterPosition();
                    notifyDataSetChanged();
                }
            });
        }
    }
    interface setTime
    {
        void onSetTime(String time);
    }

}
