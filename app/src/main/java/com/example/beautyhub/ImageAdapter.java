package com.example.beautyhub;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

class ImageAdapter extends RecyclerView.Adapter<ImageAdapter.Holder> {
String [] description;
    String [] style;
    String [] services;
    String[] price;
    String []time;
    Context context;


    public ImageAdapter(ImagesNextPage context) {
        this.context=context;


    }

    @NonNull
    @Override
    public ImageAdapter.Holder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view=LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.custom_image,viewGroup,false);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ImageAdapter.Holder holder, final int i) {
        Glide.with(context)
                .load(style[i])
                .into(holder.imageView);
        holder.services.setText(services[i]);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(context,Booking_Page.class);
                intent.putExtra("image",style[i]);
                intent.putExtra("services",services[i]);
                intent.putExtra("price",price[i]);
                intent.putExtra("description",description[i]);
                intent.putExtra("astimateTime",time[i]);
                context.startActivity(intent);
            }
        });


    }

    @Override
    public int getItemCount() {
        return style.length;
    }
    public void add(String[] hair, String[] data, String[] data2, String[] data3, String[] data4) {
        this.style=hair;
        this.services=data;
        this.price=data2;
        this.description=data3;
        this.time=data4;
        notifyDataSetChanged();

    }


    public class Holder extends RecyclerView.ViewHolder {
        ImageView imageView;
        TextView services;
        public Holder(@NonNull View itemView) {
            super(itemView);
            imageView=itemView.findViewById(R.id.imageView_cat);
            services=itemView.findViewById(R.id.hairstyle);
        }
    }

}

