package com.example.beautyhub;




import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;

import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;

public class History extends Fragment implements HistoryAdapter.CustomDialog {
    RecyclerView recyclerView;
    LinearLayout rderEmpty;
    FirebaseAuth mAuth;


    public static Fragment newInstance() {
        History fragment = new History();
        return fragment;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        mAuth = FirebaseAuth.getInstance();



        recyclerView = view.findViewById(R.id.recyclerView);
        rderEmpty = view.findViewById(R.id.rderEmpty);
        RecyclerView.LayoutManager manager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(manager);
       final HistoryAdapter adapter =new HistoryAdapter(this);
        recyclerView.setAdapter(adapter);

        String uid=FirebaseAuth.getInstance().getUid();

        FirebaseFirestore.getInstance().collection("booking").whereEqualTo("uid",uid).get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                                           @Override
                                           public void onComplete(@NonNull Task<QuerySnapshot> task) {
                                               if (task.getResult().size()>0) {
                                                   rderEmpty.setVisibility(View.GONE);
                                                   for (int i=0;i<task.getResult().size();i++) {
                                                       Booking booking=task.getResult().getDocuments().get(i).toObject(Booking.class);
                                                       adapter.add(booking);
                                                       adapter.notifyDataSetChanged();
                                                   }
                                               }


                                           }
                                       }
                ).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

    }




    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_fragment_two, container, false);
    }

    @Override
    public void showDialog(Booking booking) {
        ViewPage.instance(booking).show(getFragmentManager(),ViewPage.class.getName());

    }
    }








