package com.example.beautyhub;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    Button b1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_main);
        b1=findViewById(R.id.b1);
        b1.setOnClickListener(this);

    }



    @Override
    public void onClick(View v) {

        FirebaseUser user= FirebaseAuth.getInstance().getCurrentUser();
       if (user!=null)
       {
           Intent in = new Intent(MainActivity.this, HomePage.class);
           in.putExtra("loading",true);
           startActivity(in);
           finish();
       }else
       {
           Intent in = new Intent(MainActivity.this, LoginPage.class);
           startActivity(in);
           finish();
       }

    }
}
