package com.example.beautyhub;

import com.google.gson.annotations.SerializedName;


public class Register{

	@SerializedName("status")
	private String status;

	public void setStatus(String status){
		this.status = status;
	}

	public String getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"Register{" + 
			"status = '" + status + '\'' + 
			"}";
		}
}