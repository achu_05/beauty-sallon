package com.example.beautyhub;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import java.util.ArrayList;


class HistoryAdapter extends RecyclerView.Adapter<HistoryAdapter.MyHolder> {
    ArrayList<Booking>list;
    CustomDialog dialog;

    public HistoryAdapter(CustomDialog fragmentTwo){
        list= new ArrayList();
        dialog=fragmentTwo;
    }

    @NonNull
    @Override
    public MyHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.history_view, viewGroup, false);

        return new MyHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final MyHolder myHolder, final int i) {
        final Booking booking=list.get(i);

        myHolder.servicename.setText(booking.serviceType);
        myHolder.date.setText(booking.date);
        myHolder.price.setText(booking.price);
        myHolder.view1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.showDialog(booking);

            }
        });


    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public void add(Booking booking) {
        list.add(booking);
        notifyDataSetChanged();
    }

    public static class MyHolder extends RecyclerView.ViewHolder {
        TextView servicename, price,date,view1;



        public MyHolder(@NonNull View itemView) {
            super(itemView);
            servicename = itemView.findViewById(R.id.name);
            price = itemView.findViewById(R.id.price);
            date = itemView.findViewById(R.id.date);
            view1=itemView.findViewById(R.id.view);

        }

    }
    interface  CustomDialog
    {
        void showDialog(Booking booking);
    }
}

