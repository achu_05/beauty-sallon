package com.example.beautyhub;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.gson.Gson;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class CardPayment extends AppCompatActivity {
    Calendar ct;
    EditText f1, f2, f3, f4;
    String year, month;
    Button e1;
    ProgressDialog dialog;
    Spinner MySpin;
    String card_name;
    String date;
    FirebaseAuth firebaseAuth;
    Booking booking;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_card_payment);
        ct = Calendar.getInstance();
        f4 = findViewById(R.id.f4);
        f3 = findViewById(R.id.f3);
        f2 = findViewById(R.id.f2);
        f1 = findViewById(R.id.f1);
        e1 = findViewById(R.id.e1);
        dialog = new ProgressDialog(this);
        dialog.setMessage("wait a Second.....");
        dialog.setCanceledOnTouchOutside(false);
        firebaseAuth = FirebaseAuth.getInstance();
        MySpin = findViewById(R.id.m2);
        booking=new Gson().fromJson(getIntent().getStringExtra("data"),Booking.class);
        MySpin.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {


            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                String cou[] = getResources().getStringArray(R.array.card_name);
                card_name = cou[i];
                Toast.makeText(CardPayment.this, cou[i], Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        e1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String cardholdername = f1.getText().toString();
                String cardnumber = f2.getText().toString();
                String cvv = f3.getText().toString();
                if (cardholdername.isEmpty() && cardnumber.isEmpty() && cvv.isEmpty() ) {
                    Toast.makeText(CardPayment.this, "Please fill all Details", Toast.LENGTH_SHORT).show();
                } else {

                    placeOrder();

                }
            }
        });



    }
    void placeOrder(){
        dialog.show();
        FirebaseFirestore.getInstance().collection("booking").document().set(booking)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        dialog.dismiss();
                        Intent in=new Intent();
                        in.setClass(CardPayment.this,CashPage.class);
                        startActivity(in);

                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                dialog.dismiss();
                Toast.makeText(CardPayment.this, e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

}