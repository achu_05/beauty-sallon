package com.example.beautyhub;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;

import android.os.Bundle;
import android.support.annotation.NonNull;

import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.FirebaseFirestore;

import static java.lang.String.*;


public class RegisterPage extends AppCompatActivity implements View.OnClickListener {
    EditText username, password, name, contact, email, address;
    Button register;
    TextView login;
    FirebaseAuth firebaseAuth;
    FirebaseFirestore db;
    ProgressDialog dialog;
    boolean invalid = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_register_page);
        firebaseAuth = FirebaseAuth.getInstance();
        username = findViewById(R.id.username);
        password = findViewById(R.id.password);
         name = findViewById(R.id.name);
        contact = findViewById(R.id.contact);
        email = findViewById(R.id.email);
        address = findViewById(R.id.address);

        db=FirebaseFirestore.getInstance();
        dialog = new ProgressDialog(this);
        dialog.setMessage("wait a Second.....");
        dialog.setCanceledOnTouchOutside(false);


        login = findViewById(R.id.login);
        register = findViewById(R.id.register);
        login.setOnClickListener(this);
        register.setOnClickListener(this);
    }



    @Override
    public void onClick(View view) {

        switch (view.getId()) {


            case R.id.login:
                doLogin();
                break;
            case R.id.register:
                doRegister();
                break;
        }
    }


        private void doRegister() {

            String usname = username.getText().toString();
            String upass = password.getText().toString();
            String ucontact = contact.getText().toString();
            String uemail = email.getText().toString();
            String uaddress = address.getText().toString();
            if (usname.equals("")) {
                invalid = true;
                Toast.makeText(getApplicationContext(), "Enter your name",
                        Toast.LENGTH_SHORT).show();
            } else

            if(upass.isEmpty() || (upass.length() > 8) && !upass.matches("[a-zA-Z0-9]@.")){
                invalid=true;
                Toast.makeText(getApplicationContext(), "Password must be at least 8 characters", Toast.LENGTH_SHORT).show();
            } else

            if (ucontact.equals("") || ucontact.length()!=10)  {
                invalid = true;
                Toast.makeText(getApplicationContext(),
                        "Please enter your 10 digit contact no. ", Toast.LENGTH_SHORT)
                        .show();
            } else

            if (uemail.equals("") || !uemail.contains("@gmail.com")){
                invalid = true;
                Toast.makeText(getApplicationContext(),
                        "Please enter valid email address", Toast.LENGTH_SHORT)
                        .show();

            } else if (uaddress.equals("")) {
                invalid = true;
                Toast.makeText(getApplicationContext(),
                        "Please enter your address", Toast.LENGTH_SHORT)
                        .show();
            }
            else {
                final User user = new User();
                user.setName(usname);
                user.setPass(upass);
                user.setAddress(uaddress);
                user.setMobile(ucontact);
                user.setEmail(uemail);
                user.setImage("https://elitetix.com/upload/event/orig/project_16898.jpeg");

                dialog.show();
                firebaseAuth.createUserWithEmailAndPassword(uemail, upass)
                        .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                            @Override
                            public void onComplete(@NonNull Task<AuthResult> task) {
                                if (task.isSuccessful()) {
                                    FirebaseUser firebaseUser = task.getResult().getUser();
                                    user.setUid(firebaseUser.getUid());
                                    saveDatabse(user);
                                }
                            }
                        }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        dialog.dismiss();
                        Toast.makeText(RegisterPage.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });

            }
        }

    private void saveDatabse(User user) {
        db.collection("users")
                .document(user.getUid()).set(user)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        dialog.dismiss();
                        Intent in=new Intent(RegisterPage.this,HomePage.class);
                        in.putExtra("loading",true);
                        in.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(in);
                        finish();
                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                dialog.dismiss();
                Toast.makeText(RegisterPage.this, e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }


    private void doLogin() {
        Intent ii = new Intent();
        ii.setClass(RegisterPage.this, LoginPage.class);
        startActivity(ii);
    }


}




