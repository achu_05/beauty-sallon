package com.example.beautyhub;

import android.app.Dialog;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CalendarView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.FirebaseFirestore;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class booking_next_page extends AppCompatActivity implements ButtonAdapter.setTime{
    String time[]={"9:00 A.M","10:00 A.M","11:00 A.M","12:00 P.M","1:00 PM","2:00 PM","3:00 PM","4:00 PM","5:00 P.M"};
    TextView textView;
    Button bookorder;
    String service_time="";
    String service_date="";
    String service_type;
    String price;
    FirebaseAuth mAuth;
    RecyclerView recyclerView;
    FirebaseFirestore db;


    Toolbar toolbar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_booking_next_page);
        mAuth = FirebaseAuth.getInstance();
        textView = findViewById(R.id.textView);
        bookorder=findViewById(R.id.bookorder);
        toolbar=findViewById(R.id.toolbar);

        toolbar.setTitle("Book Appointment");
        toolbar.setTitleTextColor(Color.WHITE);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
                finish();
            }
        });


        db=FirebaseFirestore.getInstance();
        service_date=textView.getText().toString();

        bookorder.setOnClickListener(new View.
                OnClickListener() {
            @Override
            public void onClick(View view) {
                if (service_time.isEmpty() ||service_date.isEmpty() )
                {
                    Toast.makeText(booking_next_page.this, "Select date and timeList", Toast.LENGTH_SHORT).show();
                }else {
                    final Booking booking = new Booking(
                            service_time,
                            service_date,
                            service_type,
                            price);
                    FragmentManager fm = getSupportFragmentManager();
                    Payment payment = Payment.newInstance(booking);
                    payment.show(fm, "payment");

                }

            }
        });
        CalendarView calendarView = findViewById(R.id.calenderView);
        recyclerView=findViewById(R.id.recyclerView);
        service_type=getIntent().getStringExtra("service");
        price=getIntent().getStringExtra("price");

        Calendar calendar = Calendar.getInstance();
        calendarView.setMinDate(calendar.getTimeInMillis());
        String date = new SimpleDateFormat("d/M/yyyy", Locale.getDefault()).format(new Date());
        textView.setText(date);
        calendarView.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
            @Override
            public void onSelectedDayChange(@NonNull CalendarView view, int year, int month, int dayOfMonth) {
                int actual_month = month + 1;
                textView.setText(dayOfMonth + "/" + actual_month + "/" + year);
                service_date=textView.getText().toString();
            }
        });

        RecyclerView.LayoutManager manager= new GridLayoutManager(booking_next_page.this,3);
        recyclerView.setLayoutManager(manager);
        ButtonAdapter adapter=new ButtonAdapter(this,time);
        recyclerView.setAdapter(adapter);

    }


    @Override
    public void onSetTime(String time) {
        service_time=time;
    }


}