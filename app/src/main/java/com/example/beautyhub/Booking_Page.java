package com.example.beautyhub;

import android.content.Intent;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

public class Booking_Page extends AppCompatActivity   {
    String service;
    String image;
    String price;
    String description;
    String astimateTime;
    Button book;

Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_booking__page);
        ImageView style = findViewById(R.id.view_style);
        toolbar=findViewById(R.id.toolbar);

        TextView textView = findViewById(R.id.servicename);
        TextView textView1 = findViewById(R.id.pricelist);
        TextView textView2=findViewById(R.id.description);
        TextView textView3=findViewById(R.id.time);

        book=findViewById(R.id.book);
        book.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        switch (view.getId()) {

                                            case R.id.book:
                                                doBooking();
                                                break;

                                        }
                                    }

                                    private void doBooking() {
                                        Intent in = new Intent(Booking_Page.this, booking_next_page.class);
                                        in.putExtra("price",price);
                                        in.putExtra("service",service);
                                        startActivity(in);
                                        finish();
                                    }
                                });

        image = getIntent().getStringExtra("image");
        service =getIntent().getStringExtra("services");
        price=getIntent().getStringExtra("price");
        description=getIntent().getStringExtra("description");
        astimateTime =getIntent().getStringExtra("astimateTime");

        Glide
                .with(this)
                .load(image)
                .into(style);
        textView.setText(service);
        textView1.setText(price);
        textView2.setText(description);
        textView3.setText(astimateTime);
        toolbar.setTitle(service);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
                finish();
            }
        });


    }


}
