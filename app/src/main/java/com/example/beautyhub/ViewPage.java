package com.example.beautyhub;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


public class ViewPage extends DialogFragment {
static Booking booking;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getDialog().setTitle("DialogFragment Demo");
        View view = inflater.inflate(R.layout.activity_view_page, container);

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        TextView name=view.findViewById(R.id.s1);
        name.setText(booking.serviceType);

        TextView date =view.findViewById(R.id.s3);
        date.setText(booking.date);

        TextView time =view.findViewById(R.id.s4);
        time.setText(booking.time);

        TextView price =view.findViewById(R.id.s5);
        price.setText(booking.price);


    }

    public static ViewPage instance(Booking boo){
        booking=boo;

        return  new ViewPage();
    }

}