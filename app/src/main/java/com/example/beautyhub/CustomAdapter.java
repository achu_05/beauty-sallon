package com.example.beautyhub;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

class CustomAdapter extends RecyclerView.Adapter<CustomAdapter.MyHolder> {
    Context ct;

    String style[];
    String title[];
    int[] image;


    LayoutInflater inflater;

    public CustomAdapter(Context ct, String title[], String style[], int[] image) {
        inflater = LayoutInflater.from(ct);
        this.title = title;
        this.style = style;
        this.image = image;
        this.ct = ct;
    }

    @NonNull
    @Override
    public MyHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.fragment_view, viewGroup, false);

        return new MyHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyHolder myHolder, final int i) {
        if (i % 2 == 0) {
            MyHolder.title.setText(title[i]);
            MyHolder.style.setText(style[i]);
            MyHolder.img.setImageResource(image[i]);
        } else {
            MyHolder.title1.setText(title[i]);
            MyHolder.style1.setText(style[i]);
            MyHolder.img.setImageResource(image[i]);
        }


        myHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(ct,ImagesNextPage.class);
                intent.putExtra("key",title[i]);
                ct.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return image.length;
    }

    public static class MyHolder extends RecyclerView.ViewHolder {
        public static TextView title, title1;
        public static TextView style, style1;
        public static ImageView img;

        public MyHolder(@NonNull View itemView) {
            super(itemView);
            title = itemView.findViewById(R.id.title);
            title1 = itemView.findViewById(R.id.title1);
            style = itemView.findViewById(R.id.style);
            style1 = itemView.findViewById(R.id.style1);
            img = itemView.findViewById(R.id.image);

        }

    }
}

