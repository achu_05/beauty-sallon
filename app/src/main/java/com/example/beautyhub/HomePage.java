package com.example.beautyhub;


import android.app.Dialog;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ProgressBar;
import android.widget.TextView;


public class HomePage extends AppCompatActivity {
    Toolbar toolbar;
    DrawerLayout mdawer;
    Toolbar mTool;
    NavigationView mnav;
    ActionBarDrawerToggle toggle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_home_page);

      toolbar=findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(true);


        boolean loading=getIntent().getBooleanExtra("loading",false);
        if (loading)
        playVideo();
        BottomNavigationView bottomNavigationView = (BottomNavigationView) findViewById(R.id.navigation);
        bottomNavigationView.setOnNavigationItemSelectedListener
                (new BottomNavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                        Fragment selectedFragment = null;
                        switch (item.getItemId()) {
                            case R.id.home_icon:
                                selectedFragment = FragmentOne.newInstance();
                                break;
                            case R.id.add_icon:
                                selectedFragment = History.newInstance();
                                break;
                            case R.id.profile_icon:
                                selectedFragment = profileFragment.newInstance();
                                break;
                            case R.id.about_icon:
                                selectedFragment = FragmentFour.newInstance();

                        }

                        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                        transaction.replace(R.id.frame, selectedFragment);
                        transaction.commit();
                        return true;
                    }
                });


        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frame, FragmentOne.newInstance());
        transaction.commit();
    }

    private void playVideo() {
        final Dialog dialog=new Dialog(this);
        dialog.setContentView(R.layout.videodialog);
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        final ProgressBar progressBar=dialog.findViewById(R.id.progressVideo);
        final TextView skip=dialog.findViewById(R.id.skip);
        skip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (progressBar!=null)
                {
                    progressBar.setVisibility(View.GONE);
                }

            }
        },5000);
    }

}