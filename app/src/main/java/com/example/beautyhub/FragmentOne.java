package com.example.beautyhub;



import android.app.Dialog;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;

import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;


public class FragmentOne extends Fragment {
    boolean isAlive=true;
    RecyclerView recyclerView;
    Toolbar toolbar;
    String title[]={"Hair","Spa","Facial","Nails"};
    String style[]={"6 Styles","6 Styles","12 Styles","4 Styles"};
    int  []image={R.drawable.hair,R.drawable.spa,R.drawable.facial,R.drawable.nails};

    public static Fragment newInstance() {
        FragmentOne fragment = new FragmentOne();
        return fragment;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {


        recyclerView=view.findViewById(R.id.recyclerView);
        RecyclerView.LayoutManager manager= new LinearLayoutManager(getContext(),LinearLayoutManager.VERTICAL,false);
        recyclerView.setLayoutManager(manager);
        CustomAdapter adapter=new CustomAdapter(getActivity(),title,style,image);
        recyclerView.setAdapter(adapter);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_fragment_one, container, false);
    }
}







