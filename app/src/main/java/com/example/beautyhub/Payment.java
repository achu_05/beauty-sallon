package com.example.beautyhub;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.gson.Gson;


public class Payment extends DialogFragment {

    TextView card,cash,btncontinue;
    ProgressDialog dialog;
    static Booking booking;

    Boolean isOnePressed = false, isSecondPlace = false;
    public static Payment newInstance(Booking bok) {
        booking=bok;
        return  new Payment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        dialog = new ProgressDialog(getContext());
        dialog.setMessage("wait a Second.....");
        dialog.setCanceledOnTouchOutside(false);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        getDialog().setTitle("DialogFragment Demo");
        View view = inflater.inflate(R.layout.fragment_payment, container);
        card=view.findViewById(R.id.card);
        cash=view.findViewById(R.id.cash);

        cash.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cash.setBackgroundColor(getActivity().getResources().getColor(R.color.colorAccent));
                cash.setTextColor(Color.WHITE);
                isOnePressed = true;
                if (isSecondPlace) {
                    card.setBackgroundColor(Color.WHITE);
                    card.setTextColor(Color.BLACK);
                    isSecondPlace = false;
                }
            }
        });

        card.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                card.setBackgroundColor(getActivity().getResources().getColor(R.color.colorAccent));
                card.setTextColor(Color.WHITE);
                isSecondPlace = true;
                if (isOnePressed) {
                    cash.setBackgroundColor(Color.WHITE);
                    cash.setTextColor(Color.BLACK);
                    isOnePressed = false;
                }

            }
        });
        btncontinue=view.findViewById(R.id.btncount);
        btncontinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(isOnePressed==true)
                {
                    placeOrder();
                }
                else if(isSecondPlace==true)
                {
                    String bokk=new Gson().toJson(booking);
                    Intent in=new Intent();
                    in.setClass(getActivity(),CardPayment.class);
                    in.putExtra("data",bokk);
                    startActivity(in);
                }
                else if(isOnePressed && isSecondPlace)
                {
                    Toast.makeText(getContext(), "Please Select One Payment Method", Toast.LENGTH_SHORT).show();
                }

            }
        });

        return view;
    }
    void placeOrder(){
        dialog.show();
        FirebaseFirestore.getInstance().collection("booking").document().set(booking)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        dialog.dismiss();
                        Intent in=new Intent();
                        in.setClass(getActivity(),CashPage.class);
                        startActivity(in);

                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                dialog.dismiss();
                Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

}
