package com.example.beautyhub;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import retrofit2.Call;

import android.widget.TextView;
import android.widget.Toast;
import android.util.Log;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

import retrofit2.Callback;
import retrofit2.Response;
import java.io.IOException;


public class LoginPage extends AppCompatActivity  implements View.OnClickListener {
    EditText t1, t2;
    TextView txtRegister;
    Button login, cancel;
    FirebaseAuth firebaseAuth;
    ProgressDialog dialog;
    boolean invalid = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_page);
        t1 = findViewById(R.id.t1);
        t2 = findViewById(R.id.t2);
        login = findViewById(R.id.login);
        cancel = findViewById(R.id.cancel);
        txtRegister=findViewById(R.id.txtRegister);

        dialog = new ProgressDialog(this);
        dialog.setMessage("wait a Second.....");
        dialog.setCanceledOnTouchOutside(false);
        firebaseAuth = FirebaseAuth.getInstance();

        login.setOnClickListener(this);
        cancel.setOnClickListener(this);
        txtRegister.setOnClickListener(this);
        SharedPreferences sp=getSharedPreferences("data",0);
        if(sp.getBoolean("checklogin",false)==true)
        {
            Intent in = new Intent(LoginPage.this, HomePage.class);
            startActivity(in);
            finish();
        }


    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.login: {
                doLogin();
                break;
            }
            case R.id.cancel: {
                Intent in = new Intent(view.getContext(), MainActivity.class);
                startActivity(in);
                finish();
                break;
            }
            case R.id.txtRegister:
            {
                Intent ii = new Intent();
                ii.setClass(LoginPage.this, RegisterPage.class);
                startActivity(ii);
                finish();
            }
        }
    }
           /* String uname = t1.getText().toString();
            String upass = t2.getText().toString();
            if (uname.isEmpty() || upass.isEmpty()) {
                Toast.makeText(LoginPage.this, "please fill all the field", Toast.LENGTH_LONG).show();
            } else {
                Call<Login> call = ApiHitter.ApiHitter().login(uname, upass);
                call.enqueue(new Callback<Login>() {
                    @Override
                    public void onResponse(Call<Login> call, Response<Login> response) {
                        if (response.body() != null)
                        {
                            //Toast.makeText(MainActivity.this, "oooo"+response.body().toString(), Toast.LENGTH_SHORT).show();
                            //Toast.makeText(LoginPage.this, "oooo" + response.body().getStatus(), Toast.LENGTH_SHORT).show();
                        SharedPreferences sp = getSharedPreferences("data", 0);
                        SharedPreferences.Editor et = sp.edit();
                        et.putBoolean("checklogin", true);
                        et.commit();//permanent store data
                        Intent in = new Intent(LoginPage.this, HomePage.class);
                        startActivity(in);
                        finish();
                    }
                        else {
                            try {
                                Toast.makeText(LoginPage.this, "ttt" + response.errorBody().string(), Toast.LENGTH_SHORT).show();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                    @Override
                    public void onFailure(Call<Login> call, Throwable t) {
                        Log.e("test1", "test1");
                        Toast.makeText(LoginPage.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });


            }

        }*
        if(view.getId() == R.id.b2)
        {
            Intent in=new Intent(view.getContext(),RegisterPage.class);
            startActivity(in);
            finish();
        }

    }*/

    private void doLogin() {
        String useremail = t1.getText().toString();
        String upassword = t2.getText().toString();
        if (useremail.isEmpty() || upassword.isEmpty()) {
            invalid = true;
            Toast.makeText(LoginPage.this, "please fill given fields", Toast.LENGTH_LONG).show();
        }
        else if(!useremail.contains("@gmail.com")){
            invalid = true;
            Toast.makeText(getApplicationContext(),
                    "Please enter valid email address", Toast.LENGTH_SHORT)
                    .show();
        }
        else if(upassword.matches("") && upassword.length()!= 8){
            invalid = true;
            Toast.makeText(getApplicationContext(), "Password must be at least 8 characters", Toast.LENGTH_SHORT).show();
        }
        else {
            dialog.show();
            firebaseAuth.signInWithEmailAndPassword(useremail, upassword)
                    .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (task.isSuccessful()) {

                                SharedPreferences sp=getSharedPreferences("data",0);
                                SharedPreferences.Editor et=sp.edit();
                                et.putBoolean("checklogin",true);
                                et.commit();//p
                                dialog.dismiss();// ermanent store data
                                    Intent in = new Intent(LoginPage.this, HomePage.class);
                                    in.putExtra("loading",true);
                                    startActivity(in);
                                    finish();
                                }
                            }

                    }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    dialog.dismiss();
                    Toast.makeText(LoginPage.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });

        }

    }
}