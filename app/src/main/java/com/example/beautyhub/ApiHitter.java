package com.example.beautyhub;

import com.example.beautyhub.LoginData.Login;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Query;

public class ApiHitter {
    public static Retrofit retrofit;
    public static String BASE_URL="http://onlinetutorial.co.in/project1/";
    public static ApiInterface ApiHitter(){
        retrofit=new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        ApiInterface apiInterface=retrofit.create(ApiInterface.class);
        return apiInterface;

    }
    interface  ApiInterface {
        // http://onlinetutorial.co.in/talent/login.php?username=9781333368&password=admin
        @GET("login.php")
        Call<Login> login(@Query("username") String username, @Query("password") String password);
        @GET("insert.php")
        Call<Register>register(
                @Query("username")String username,
                @Query("password")String password,
                @Query("name")String name,
                @Query("contact")String contact,
                @Query("email")String email,
                @Query("gender")String gender,
                @Query("address")String address,
                @Query("longitude")String Longitude,
                @Query("latitude")String latitude,
                @Query("usertype")String usertype);

    }

}
