package com.example.beautyhub;


import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import de.hdodenhof.circleimageview.CircleImageView;

public class profileFragment extends Fragment {
    TextView logout;
    TextView userName,username,email,contact,address;
    CircleImageView profileimage;
    ImageView imaBg;
    LinearLayout progressView;
    int REQUEST_CODE = 12;
    private StorageReference mStorageRef;
    ProgressDialog dialog;

    public static Fragment newInstance() {
        profileFragment fragment = new profileFragment();
        return fragment;
    }

    @Override
    public void onResume() {
        super.onResume();
        getData();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {

        mStorageRef = FirebaseStorage.getInstance().getReference();
        profileimage =view.findViewById(R.id.profileimage);
        imaBg =view.findViewById(R.id.imaBg);
        progressView =view.findViewById(R.id.progressView);
        profileimage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                imagerPicker();
            }

        });

        logout = view.findViewById(R.id.logout);
        userName = view.findViewById(R.id.userName);
        username=view.findViewById(R.id.username);
        email=view.findViewById(R.id.email);
        contact=view.findViewById(R.id.contact);
        address=view.findViewById(R.id.profileaddresss);
        dialog = new ProgressDialog(getContext());
        dialog.setMessage("wait a Second.....");
        dialog.setCanceledOnTouchOutside(false);

        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferences sp = getContext().getSharedPreferences("data", 0);
                SharedPreferences.Editor et = sp.edit();
                et.putBoolean("checklogin", false);
                FirebaseAuth.getInstance().signOut();
                et.commit();//permanent store data
                Intent intent = new Intent(getActivity(), MainActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);




            }
        });


    }

    private void imagerPicker() {
        Intent in = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(in, REQUEST_CODE);

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE) {
            Uri path = data.getData();
            uploadImage(path);
        }
    }

    private void uploadImage(Uri path) {
        dialog.show();
        final StorageReference ref = mStorageRef.child("users/"+FirebaseAuth.getInstance().getUid()+".jpg");
        UploadTask uploadTask = ref.putFile(path);

       uploadTask.continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
            @Override
            public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                if (!task.isSuccessful()) {
                    throw task.getException();
                }

                // Continue with the task to get the download URL
                return ref.getDownloadUrl();
            }
        }).addOnCompleteListener(new OnCompleteListener<Uri>() {
            @Override
            public void onComplete(@NonNull Task<Uri> task) {
                if (task.isSuccessful()) {
                    Uri downloadUri = task.getResult();
                    Toast.makeText(getContext(), downloadUri.toString(), Toast.LENGTH_SHORT).show();
                    updateProfile(downloadUri);
                }
            }
        }).addOnFailureListener(new OnFailureListener() {
           @Override
           public void onFailure(@NonNull Exception e) {
               dialog.dismiss();
               Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_SHORT).show();
           }
       });
    }

    private void updateProfile(final Uri downloadUri) {
        FirebaseFirestore.getInstance().collection("users").document(FirebaseAuth.getInstance().getUid())
                .update("image",downloadUri.toString())
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        dialog.dismiss();
                        Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        dialog.dismiss();
                    }
                },4000);
                Glide.with(getActivity())
                        .load(downloadUri)
                        .into(profileimage);
                Glide.with(getActivity())
                        .load(downloadUri)
                        .into(imaBg);
            }
        });
    }



    private void getData() {

        FirebaseFirestore.getInstance().collection("users").document(FirebaseAuth.getInstance().getUid()).get()
                .addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                        if (task.isSuccessful())
                        {
                           try {
                               User user=task.getResult().toObject(User.class);
                               Glide.with(getContext()).load(user.image)
                                       .placeholder(R.drawable.plm)
                                       .into(profileimage);
                               Glide.with(getContext())
                                       .load(user.image)
                                       .placeholder(R.drawable.plm)
                                       .into(imaBg);
                               userName.setText(user.name);
                               username.setText("Name:- "+user.name);
                               email.setText("E-mail:- "+user.email);
                               contact.setText("Mobile:- "+user.mobile);
                               address.setText("Address:- "+user.address);
                               new Handler().postDelayed(new Runnable() {
                                   @Override
                                   public void run() {

                                       progressView.setVisibility(View.GONE);
                                   }
                               },2000);
                           }catch (Exception e){}
                        }
                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
dialog.dismiss();
            }
        });


    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_fragment_three, container, false);
        return view;
    }
}