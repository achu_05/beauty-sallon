package com.example.beautyhub;

import android.content.pm.ActivityInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

public class ImagesNextPage extends AppCompatActivity {
    String title;

    String hairprice[]={"Rs.2500","Rs.2500","Rs.600","Rs.400","Rs.300","Rs.500"};
    String hairservices[]={"Hair Smoothing","Hair Rebonding"," Hair spa"," Hair Cleansing","Hair Cut","Hair Colour"};
    String hair[]={"https://cdn2.stylecraze.com/wp-content/uploads/2013/10/11-Side-Effects-Of-Hair-Smoothing.jpg",
    "https://www.boldsky.com/img/2018/10/cover-1540987810.jpg",
    "https://www.mostinside.com/wp-content/uploads/2017/05/Advantages-of-Hair-Spa.png",
    "https://qph.fs.quoracdn.net/main-qimg-7c4a8d3340c27d793ed726c67e478e83",
    "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQJwLNOFHuf749_7j5h0iJffqIBgO683QNGzNyW78Mjbks2RFR9",
    "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSTMbA_z6L5d-eah9-YcLRsqUuT1nnngfxby9CB8Osw5Z_iB7Ww"};
    String hairDescription[]={"Hair smoothing is a temporary treatment that also goes by the names Brazilian blowout, keratin treatment, and protein treatment. The process involves saturating your hair with a formaldehyde solution followed by drying it out and using a flat iron to lock your hair in a straight position",
   "While textured hair looks gorgeous, not a lot of people realize how much work and effort goes into keeping it tame. This is why a lot of people opt to get their hair rebonded. Not only does this treatment help make textured hair smooth, shiny, and easy to maintained",
   "A hair spa treatment is depicted as “hair rebirth therapy,” where your hair is nourished and conditioned with ingredients that assistance turn around damage and boost hair wellbeing, abandoning you with hair that feels like it has never experienced any sort of damage.",
   "Hair cleansing  is the cosmetic act of keeping hair clean by washing it with shampoo or other detergent products and water. Hair conditioner may also be used to improve hair's texture and manageability. Two-in-one shampoos, which have both detergent and conditioning components, are now commonly also used as a replacement for shampoo and conditioner.",
   "A haircut refers to the styling of hair, usually on the human scalp. Sometimes, this could also mean an editing of facial or body hair. The fashioning of hair can be considered an aspect of personal grooming, fashion, and cosmetics, although practical, cultural, and popular considerations also influence some hairstyles.",
    "Hair coloring, or hair dyeing, is the practice of changing the hair color. The main reasons for this are cosmetic: to cover gray or white hair, to change to a color regarded as more fashionable or desirable, or to restore the original hair color after it has been discolored by hairdressing processes or sun bleaching."};
String htime[]={"4hrs","4hrs","45min","60min","20min","30min"};

    String spaprice[]={"Rs.2000","Rs.1900","Rs.2600","Rs.3500","Rs.3000","Rs.3800"};
    String spaservies[]={"Indonesia spa","Choclate spa","Day spa ","Aroma Theraphy","Esential Oil Spa","Japnese Spa"};
    String spa[]={"https://www.collectoffers.com/EditorImages/Banner-1_DxOFP.jpg",
            "https://www.breslowmd.com/wp-content/uploads/2018/02/chocolate-spa-e1517596067263.jpg",
    "https://sentres-cdn1-456069.c.cdn77.org/photos/460714/slider_large/day-spa---hotel-mirabell-avelengo--hafling--merano-meran-and-environs-south-tyrol.jpg",
    "https://images.pexels.com/photos/3188/love-romantic-bath-candlelight.jpg?auto=compress&cs=tinysrgb&dpr=1&w=500","http://www1.pictures.zimbio.com/mp/zBpgm6eZNEGx.jpg",
 "https://hips.hearstapps.com/hmg-prod.s3.amazonaws.com/images/nordic-sauna-1516036683.jpg"};
    String spaDescription[]={"Indonesian massage is a full-body, deep-tissue, holistic treatment. Indonesian massage uses a combination of gentle stretches, acupressure, reflexology, and aromatherapy to stimulate the flow of blood, oxygen and \"qi\" (energy) around your body, and bring a sense of wellbeing, calm and deep relaxation.",
    "In Choclate spa the body will exfoliated with ground cocoa nibs, then slathered in a creamy, cocoa-based mixture.Then you will full-body wrapped in plastic, layered with banana leaves and covered with blankets to bring to your body temperature up, and which leads to get your stress level down.",
    "A day spa is a business that provides a variety of services for the purpose of improving health, beauty and relaxation through personal care treatments such as hair, massages and facials. A day spa is different from a beauty salon in that it contains facilities such as a sauna, pool, steam room, or whirlpool that guests may use in addition to their treatment",
    "Aromatherapy is normally used through inhalation or as a topical application.The oils evaporate into the air using a diffuser container, spray, or oil droplets, or breathed in.Apart from providing a pleasant smell, aromatherapy oils can provide respiratory disinfection, decongestant, and psychological benefits.",
    "Relaxing essential oils have been used in small to high profile spas around the world. They are not only diffused to experience holistic benefits, but they are also added to topical applications such as skin care products, massage oils, and spa treatments.",
    "An onsen is a Japanese hot spring; the term also extends to cover the bathing facilities and traditional inns frequently situated around a hot spring. As a volcanically active country, Japan has thousands of onsens scattered throughout all of its major islands."};
String stime[]={"2hrs","2hrs","2hrs","2hrs","2hrs","2hrs"};

    String facialprice[]={"Rs.700","Rs.800","Rs.900","Rs.800","Rs.800","Rs.900","Rs.900","Rs.1200","Rs.1200","Rs.2000","Rs.1000","Rs.900"};
    String facialservices[]={"Fruit Facial","Aroma Facial","Oxygen Facial","Silver Facial","Pearl Facial","Choclate Facial","Lotus Facial","Gold Facial"
            ,"Diamond Facial","Skin Lightening Facial","De Tan Facial","VLCC Facial"};
    String facial[]={"https://stylesatlife.com/wp-content/uploads/2014/01/Fruit-Face-mask.jpg",
    "http://holisticbeautylanzarote.com/wp-content/uploads/2015/01/bigstock-Facial-Treatment-33619751-300x200.jpg",
            "http://www.avalaser.com/wp-content/uploads/oxygen-facial-benefits.jpg",
    "https://cdn.shopify.com/s/files/1/1084/9136/products/diamond_mask_2.jpg?v=1498452832",
    "http://www.srisbeauty.in/wp-content/uploads/2017/11/PearlFacial-e1510470516226.jpg","https://img.grouponcdn.com/deal/2LmDgkyy5vXJKFHQ5HjFNdtvDi3Q/2L-1000x600/v1/c700x420.jpg",
    "http://www.smedunia.in/sites/default/files/products/image/441672/Lotus-Facial-700.jpg",
    "https://yabeauty.com/wp-content/uploads/2017/09/goldmaskfacialbenefits.png",
    "https://i9.dainikbhaskar.com/thumbnails/680x588/web2images/english.fashion101.in/2018/03/05/untitled_1520231793.jpg",
    "https://img.wikinut.com/img/3x2m4amdyzwjvgov/jpeg/0/Facial-Skin-Care.jpeg","https://bizimages.withfloats.com/actual/5be26ff99be7ce0001a1e144.jpg",
    "http://stat.homeshop18.com/homeshop18/images/productImages/196/vlcc-facial-dhamaka-offer-medium_f2c5da71dbcea070dfb092053a0d198a.jpg"};
String facialDescription[]={"fruits are natural exfoliating agents. The natural scrub gently removes the top layer and all trapped impurities, leaving behind gorgeous skin. Stubborn pores get unclogged with fruit facials. Say goodbye to acne, blackheads, whiteheads and pimples.",
"The aromatherapy facial ideally begins with a facial steam, which cleans the pores thoroughly. It also cleans the impurities that have accumulated over the astimateTime At the same astimateTime, drops of essential oils that have been added to the water, work deep within the pores of your skin",
"Oxygen is suggested to strengthen skin's elasticity and help eliminate acne-causing bacteria, as well as reduce fine lines and wrinkles, even out skin tone, and diminish pores. ",
"This facial is done to detoxify and purify your skin. The silver facial consists of a glow scrub, gel, cream and pack that offers dull skin an instant lift.It clears the pores and deep cleans to prevent the formation of blackheads.",
"Pearl dust combined with pure essential oil blends hydrates & softens the skin adding a natural pearl like luminosity. It improves skin texture, lightens & brightens skin tone.",
"Rich in antioxidants, chocolate helps reverse the signs of ageing.This Chocolate facial gives you instant Skin Brightening and Naturally Glow , It moisturizes your skin, protects the sun damage and tightens by maintaining the collagen production.",
"Lotus facial kit is just perfect as it cleans and exfoliates dull layer of skin. It lightens, tones, firms and nourishes the skin. It also rejuvenates dull and dehydrated skin. Makes skin soft and supple.",
" A gold facial is honestly one of the most indulgent spa treatments we can think of But while they were once a pampering method reserved only for celebrities and ancient royals and gold face masks popping up on high-end spa menus everywhere.",
"Diamond facial kit will leave your skin polished, cleansed, and feeling fresh due to the unique formula that contains rich, natural active ingredients. The Diamond scrub exfoliates dead skin cells and goes deep into the skin to remove blackheads and whiteheads.",
"The goal of skin whitening (or skin bleaching) is to lighten or fade the complexion beyond the natural skin tone.",
"It is a process to remove the tanned skin to even out the skin tone. There are numerous ways to de-tan, such as magical serums, effective peeling, bleaching of the skin, de-tan facials and body wraps and our favourite natural organic therapies.",
"Luxurious facial that improves skin's hydration and metabolism, prevents premature aging, removes pigmentation, age spots, lines and wrinkles, boosts collagen regeneration, and strengthens elastin fibres to reveal radiant, luminous, youthful complexion."};
String ftime[]={"15min","10min","13min","15min","20min","30min","25min","20min","10min","17min","15min","10min"};


    String nailsprice[]={"Rs.400","Rs.200","Rs.150","Rs.120"};
    String nailsservices[]={"Illuminate Nair Art","Nail Art Extension","Nail Refilling","Polish Change"};
    String nails[]={"https://i.pinimg.com/originals/80/82/73/808273267790ee5504115e1d12f3a3f2.jpg",
    "http://saundaryam.net/wp-content/uploads/2017/07/nail2-min.jpg","http://lanailsbc.com/nails/wp-content/uploads/2017/04/refill-icon.jpg",
    "https://curage.ca/wp-content/uploads/sites/5/2016/05/HandCare-Polish-Change.jpg"};
    String nailsDescription[]={"Nail art is a creative way to paint, decorate, enhance, and embellish the nails.It is a type of artwork that can be done on fingernails and toenails, usually after manicures or pedicures. A manicure and a pedicure are beauty treatments that trim, shape, and polish the nail.Often these procedures remove the cuticles and soften the skin around the nails.Types of manicures can vary from polish on natural nails, dipping powder, and acrylic nails.",
    "These playful nail polishes change color based on your body temperature, your mood, the heat of your cup of coffee, oreven while you're holding an ice cold lemonade!In The Mood colors will continue changing back and forth for as long as you wear them. Don't worry - you're not crazy, but your nail polish, on the other hand may be described as unstable. Finally, a nail polish that will reflectyour inner hysteria. Amuse yourself by guessing what shade your nail polish will be at any given moment.In The Mood has been featured in many publications, including Glamour, Instyle Magazine and Boston Herald.",
    "acrylics to the exposed nail bed. Acrylic nail refill is much simpler than doing. a whole new set of nails. When acrylics dry, clean cuticle with nail e file, shape, and buff smooth.During a refill we file off the existing colour and remove any lifting. We will have a quick look at the nail to make sure there is no signs of bacteria orinfections. You can choose to change the shape of the nail or take the length down.We then fill in the gap that has appeared around the cuticle area as your nails have grown with more gel/acrylic.Once hardened, we then file and shape the nails to perfection and apply a new coat of colour that you have chosen.",
    "Nail art is a creative way to paint, decorate, enhance, and embellish the nails. It is a type of artwork that can be done on fingernails and toenails, usually after manicures or pedicures.A manicure and a pedicure are beauty treatments that trim, shape, and polish the nail."};
     String ntime[]={"15min","20min","30min","25min"};
    RecyclerView recyclerView;
    Toolbar toolbar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_images_next_page);
        recyclerView=findViewById(R.id.recyclerView);
        toolbar=findViewById(R.id.toolbar);

        title=getIntent().getStringExtra("key");
        RecyclerView.LayoutManager manager= new GridLayoutManager(ImagesNextPage.this,2);
        recyclerView.setLayoutManager(manager);
        ImageAdapter adapter=new ImageAdapter(this);
        recyclerView.setAdapter(adapter);

        if (title.equals("Hair"))
        {
            toolbar.setTitle("Hair Services");

            adapter.add(hair,hairservices,hairprice,hairDescription,htime);
            adapter.notifyDataSetChanged();
        }else if (title.equals("Spa"))
        {
            toolbar.setTitle("Spa Services");
            adapter.add(spa, spaservies, spaprice, spaDescription, stime);
            adapter.notifyDataSetChanged();
        }
        else if(title.equals("Facial"))
        {
            toolbar.setTitle("Facial Services");
            adapter.add(facial, facialservices, facialprice, facialDescription, ftime);
            adapter.notifyDataSetChanged();
        }

      else if(title.equals("Nails"))
        {
            toolbar.setTitle("Nails Services");
            adapter.add(nails, nailsservices, nailsprice, nailsDescription, ntime);
            adapter.notifyDataSetChanged();
        }

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
                finish();
            }
        });
    }
}
